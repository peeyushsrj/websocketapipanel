'use strict';

angular.module('apiboard', [])
.controller('main', mainCtrl);


function mainCtrl($scope) {
	var conn = new WebSocket("ws://localhost:8890/ws");
	conn.onopen = function(e){
		console.log("Connection Opened")
	}
	conn.onerror = function (e){
		console.log(e)
	}
	conn.onmessage = function(evt) {
		$scope.resp = {}
		var resp = JSON.parse(JSON.parse(evt.data));
		console.log(resp);
		$scope.resp = resp;
		$scope.$apply()
	}
	conn.onclose = function(e){
	    //for breaking front when server fail
	    console.log("Connection Closed")
	    location.reload()
	}  
};