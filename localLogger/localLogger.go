package localLogger

import (
	"log"
	"time"

	"gopkg.in/mgo.v2"
)

type RequestLog struct {
	// Id           int
	Method       string
	RequestTime  time.Time
	ResponseTime int
}

func Append(method string, l RequestLog) {
	session, err := mgo.Dial("localhost")
	if err != nil {
		panic(err)
	}
	defer session.Close()
	err = session.DB("request").C(method).Insert(&l)
	if err != nil {
		log.Fatal(err)
	}
}

func Load(lg map[string][]RequestLog) (map[string][]RequestLog, error) {
	session, err := mgo.Dial("localhost")
	if err != nil {
		return nil, err
	}
	defer session.Close()
	l := []RequestLog{}
	err = session.DB("request").C("GET").Find(nil).All(&l)
	if err != nil {
		return nil, err
	}
	lg["GET"] = append(lg["GET"], l...)

	l = nil
	err = session.DB("request").C("POST").Find(nil).All(&l)
	if err != nil {
		return nil, err
	}
	lg["POST"] = append(lg["POST"], l...)

	err = session.DB("request").C("PUT").Find(nil).All(&l)
	if err != nil {
		return nil, err
	}
	lg["PUT"] = append(lg["GET"], l...)

	l = nil
	err = session.DB("request").C("DELETE").Find(nil).All(&l)
	if err != nil {
		return nil, err
	}
	lg["DELETE"] = append(lg["POST"], l...)
	return lg, nil
}
