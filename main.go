package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"math/rand"
	"net/http"
	"strings"
	"time"

	"github.com/gorilla/websocket"
	"github.com/rs/cors"

	logger "./localLogger"
)

var activeReqs map[string]int
var reqLog map[string][]logger.RequestLog
var upgrader = websocket.Upgrader{}

type ProcessEndPointResponse struct {
	Time     time.Time           `json:"time"`
	Method   string              `json:"method"`
	Headers  map[string][]string `json:"headers"`
	Path     string              `json:"path"`
	Query    string              `json:"query"`
	Body     string              `json:"body"`
	Duration int                 `json:"duration"`
}

type SocketMsg struct {
	Total    map[string]Prop `json:"total"`
	Active   map[string]int  `json:"active"`
	LastHour map[string]Prop `json:"lasthour"`
	LastMin  map[string]Prop `json:"lastminute"`
}

type Prop struct {
	Count int
	Time  float32
}

func processHandler(rw http.ResponseWriter, req *http.Request) {
	if len(activeReqs) == 0 {
		activeReqs = make(map[string]int)
		activeReqs[req.Method] = 1
	} else {
		activeReqs[req.Method] = activeReqs[req.Method] + 1
	}

	//Headers Lowercased as given
	headersLowerCased := http.Header{}
	for k, v := range req.Header {
		headersLowerCased[strings.ToLower(k)] = v
	}

	//Reading & Writing Request Body
	bodyData, _ := ioutil.ReadAll(req.Body)
	defer req.Body.Close()

	// Time-out for Process
	time_out := rand.Intn(15) + 15
	//time_out := rand.Intn(5)
	time.Sleep(time.Duration(time_out) * time.Second)

	//Response Preparing
	p1 := &ProcessEndPointResponse{
		time.Now(),
		req.Method,
		headersLowerCased,
		req.URL.Path,
		req.URL.RawQuery,
		string(bodyData),
		time_out}

	//JSON Response & then printing as response
	json_data, err := json.Marshal(p1)
	if err != nil {
		log.Fatal(err)
	}
	rw.Write(json_data)

	//DATA LOGGING
	log := logger.RequestLog{p1.Method, p1.Time, p1.Duration}
	//Writing to stdout
	fmt.Println(log)
	//Writing to memory
	reqLog[p1.Method] = append(reqLog[p1.Method], log)
	//Writing to database
	logger.Append(p1.Method, log)

	//de-activating request
	activeReqs[req.Method] = activeReqs[req.Method] - 1

}

func statsHandler(w http.ResponseWriter, r *http.Request) {
	http.ServeFile(w, r, "api.html")
}

func wsHandler(w http.ResponseWriter, r *http.Request) {
	c, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Print("upgrade:", err)
		return
	}
	defer c.Close()

	var msg = SocketMsg{}
	// lastActiveCount := -1

	for {
		//Efficient mechanism
		time.Sleep(1 * time.Second)
		// activeCount := count(activeReqs)

		// if activeCount != lastActiveCount || len(msg.Total) == -1 { //it means change
		// lastActiveCount = activeCount
		msg = SocketMsg{
			computeTotalRequests(),
			computeActiveRequests(),
			computeLastHourRequests(),
			computeLastMinuteRequests(),
		}
		data, err := json.Marshal(&msg)
		if err != nil {
			log.Fatal(err)
		}
		c.WriteJSON(string(data))
		// }
	}
}

func count(m map[string]int) int {
	value := 0
	for _, v := range m {
		value = value + v
	}
	return value
}

func main() {
	//Loading previous logs from database
	reqLog = make(map[string][]logger.RequestLog)
	reqLog, err := logger.Load(reqLog)
	if err != nil {
		log.Println(err)
	}
	fmt.Println(len(reqLog))
	//Route handlers
	mux := http.NewServeMux()
	mux.HandleFunc("/process/", processHandler)
	mux.HandleFunc("/stats", statsHandler)
	mux.HandleFunc("/ws", wsHandler)
	mux.Handle("/", http.FileServer(http.Dir(".")))
	fmt.Println("Running on http://localhost:8890")
	handler := cors.Default().Handler(mux)
	err = http.ListenAndServe(":8890", handler)
	if err != nil {
		log.Fatal(err)
	}
}

func computeTotalRequests() map[string]Prop {
	tl := make(map[string]Prop)
	for k, v := range reqLog {
		pr := Prop{}
		pr.Count = len(v)
		pr.Time = computeAverage(v)
		tl[k] = pr
	}
	return tl
}

func computeActiveRequests() map[string]int {
	ar := make(map[string]int)
	for k, v := range activeReqs {
		ar[k] = v
	}
	return ar
}

func computeLastHourRequests() map[string]Prop {
	lastHour := time.Now().Add(-1 * time.Hour)
	lhr := make(map[string]Prop)
	for k, v := range reqLog {
		temp1 := make([]logger.RequestLog, 0)
		for _, elog := range v {
			if elog.RequestTime.After(lastHour) {
				temp1 = append(temp1, elog)
			}
		}
		pr := Prop{}
		pr.Count = len(temp1)
		pr.Time = computeAverage(temp1)
		lhr[k] = pr
	}
	return lhr
}

func computeLastMinuteRequests() map[string]Prop {
	lastMinute := time.Now().Add(-1 * time.Minute)
	lmr := make(map[string]Prop)
	for k, v := range reqLog {
		temp1 := make([]logger.RequestLog, 0)
		for _, elog := range v {
			if elog.RequestTime.After(lastMinute) {
				temp1 = append(temp1, elog)
			}
		}
		pr := Prop{}
		pr.Count = len(temp1)
		pr.Time = computeAverage(temp1)
		lmr[k] = pr
	}
	return lmr
}

func computeAverage(plog []logger.RequestLog) float32 {
	num := 0
	for _, resp := range plog {
		num = num + resp.ResponseTime
	}
	var avg float32
	if len(plog) == 0 {
		avg = 0
	} else {
		avg = float32(num) / float32(len(plog))
	}
	return avg
}
